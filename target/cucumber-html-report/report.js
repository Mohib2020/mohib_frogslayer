$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Paramount.feature");
formatter.feature({
  "line": 2,
  "name": "Paramount",
  "description": "",
  "id": "paramount",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@run"
    }
  ]
});
formatter.scenario({
  "comments": [
    {
      "line": 3,
      "value": "#Adding a new project"
    }
  ],
  "line": 6,
  "name": "Verify user is able to add a new project",
  "description": "",
  "id": "paramount;verify-user-is-able-to-add-a-new-project",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 5,
      "name": "@Paramount"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "I navigate to LoginPage",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I enter Email and Password",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I click on Login button",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "I am on paramount dashboard",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I click on ADD PROJECT",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});