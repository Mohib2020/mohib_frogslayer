@run
Feature: Paramount & CO Adding and Searching for a Project

  @Login_Logout
  Scenario: Verify user is able to Login and Logout
    Given I navigate to LoginPage
    When I enter Email and Password
    Then I click on Login button
    Then I wait for few milliseconds
    Then I click on Logout
    And I close the browser window

  @Add_Project
  Scenario: Verify user is able to add a Project
    Given I navigate to LoginPage
    When I enter Email and Password
    Then I click on Login button
    Then I wait for few milliseconds
    Then I click on ADD PROJECT
    And I enter Project Name in the text box
    And I submit the form and click on Save button
    Then I click on Logout
    And I close the browser window

  @Search_Project
  Scenario: Verify user is able to search for a Project
    Given I navigate to LoginPage
    When I enter Email and Password
    Then I click on Login button
    Then I wait for few milliseconds
    And I enter Project Name in the search box
    And I click on Search button
    And I click on Go To Project
    Then I click on Logout
    And I close the browser window

  @End_2_End_Scenario
  Scenario: Verify user is able to add a new project
    Given I navigate to LoginPage
    When I enter Email and Password
    Then I click on Login button
    Then I wait for few milliseconds
    Then I click on ADD PROJECT
    And I enter Project Name in the text box
    And I submit the form and click on Save button
    And I enter Project Name in the search box
    And I click on Search button
    And I click on Go To Project
    Then I click on Logout
    And I close the browser window