package Binding;

import cucumber.api.PendingException;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;

public class Paramount {

    WebDriver driver;

    @Given("^I navigate to LoginPage$")
    public void i_navigate_to_LoginPage() throws Throwable {

        System.setProperty("webdriver.firefox.driver","C://edge//firefoxdriver.exe");
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get("http://paramount-demo.frogslayerdev.com");
        driver.manage().window().maximize();
    }
    @When("^I enter Email and Password$")
    public void i_enter_Email_and_Password() throws Throwable {
        driver.findElement(By.xpath("//input[@type='email']")).sendKeys("larry.test@frogslayer.com");
        driver.findElement(By.xpath("//input[@type='password']")).sendKeys("P@ssword1");
    }
    @Then("^I click on Login button$")
    public void i_click_on_Login_button() throws Throwable {
        driver.findElement(By.xpath("//button[text()='Login']")).click();
    }
    @Then("^I click on ADD PROJECT$")
    public void i_click_on_ADD_PROJECT() throws Throwable {
        WebElement ele = driver.findElement(By.xpath("//div[@id='sub-header']/following::a[text()[normalize-space()='Add Project']]"));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click()", ele);
        Thread.sleep(4000);
    }
    @Then("^I enter Project Name in the search box$")
    public void i_enter_Project_Name_in_the_search_box() throws Throwable{
        driver.findElement(By.xpath("//input[@id='search']")).sendKeys("Mohib_Project");
        Thread.sleep(4000);
    }
    @Then("^I wait for few milliseconds$")
    public void i_wait_for_few_milliseconds() throws Throwable {
        Thread.sleep(6000);
    }
    @Then("^I click on Logout$")
    public void i_click_on_Logout() throws Throwable {
        driver.findElement(By.xpath("(//a[@class='btn navbar-btn'])[2]")).click();
        Thread.sleep(3000);
    }
    @Then("^I click on Search button$")
    public void i_click_on_Search_button() throws Throwable {
        driver.findElement(By.xpath("//button[@class='btn-primary']")).click();
        Thread.sleep(4000);
    }
    @Then("^I close the browser window$")
    public void i_close_the_browser_window() throws Throwable {
        driver.close();
    }
    @Then("^I enter Project Name in the text box$")
    public void i_enter_Project_Name_in_the_text_box() throws Throwable {
        driver.findElement(By.xpath("//input[@title='This field is required.']")).sendKeys("Mohib_Project");
        driver.findElement(By.xpath("//input[@data-bind='value: surveyCost']")).sendKeys("20.00");
        driver.findElement(By.xpath("//input[@data-bind='value: travelCost']")).sendKeys("100.00");
        Thread.sleep(3000);
    }
    @Then("^I submit the form and click on Save button$")
    public void i_click_on_Save_button_and_submit_the_form() throws Throwable {
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        Thread.sleep(4000);
    }
    @Then("^I click on Go To Project$")
    public void i_click_on_Go_To_Project() throws Throwable {
        driver.findElement(By.xpath("//a[contains(text(),'Go To Project')]")).click();
        Thread.sleep(3000);
    }
}